// Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
// 实现思路:
//根据这个效果的实际需求，可以提取到两个关键点，一个是光路的生成，一个是光路随着时间进行偏移。
//直观可以看出光路由两根斜率一样的直线组成，其中一根在x轴上偏移一定的距离，
//两根斜线就能够组成一个倾斜的区域，这个区域用数学来表达就是：
//两根斜线形成的不等式组。直线的斜截式方程是`y = kx + b`，假设斜率k为1，
//那光路的区域就可以表示为：`x >= -y` 和 `x <= -y + width`，其中`width`就是我们定义的光路的宽度，
//有了区域之后我们只需要让符合该区域的像素点色彩叠加点变化就可以实现光路的效果。
// shader 原项目地址 : https://github.com/ifengzp/cocos-awesome

CCEffect %{
  techniques:
  - passes:
    - vert: vs
      frag: fs
      blendState:
        targets:
        - blend: true
      rasterizerState:
        cullMode: none
      properties:
        texture: { value: white }
        width : { value : 0.1 , editor: { tooltip: '光泽宽度,取值0~1' } }
        strength : { value : 1.2 , editor: { tooltip: '光泽亮度' }}
}%


CCProgram vs %{
  precision highp float;

  #include <cc-global>
  #include <cc-local>

  in vec3 a_position;
  in vec4 a_color;
  out vec4 v_color;

  #if USE_TEXTURE
  in vec2 a_uv0;
  out vec2 v_uv0;
  #endif

  void main () {
    vec4 pos = vec4(a_position, 1);

    #if CC_USE_MODEL
    pos = cc_matViewProj * cc_matWorld * pos;
    #else
    pos = cc_matViewProj * pos;
    #endif

    #if USE_TEXTURE
    v_uv0 = a_uv0;
    #endif

    v_color = a_color;

    gl_Position = pos;
  }
}%


CCProgram fs %{
  precision highp float;

  #include <alpha-test>
  #include <texture>
  #include <cc-global>

  in vec4 v_color;

  #if USE_TEXTURE
  in vec2 v_uv0;
  uniform sampler2D texture;
  #endif

  uniform ARGS{
    float width;
    float strength;
  };

  void main () {
    vec4 color = vec4(1, 1, 1, 1);
    color *= texture(texture, v_uv0);

    float k = 0.2;
    float time_step = -width;
    time_step += mod(cc_time.x, 1.0 + 2.0 * width);

    if (v_uv0.x >= -v_uv0.y * k + time_step && v_uv0.x <= -v_uv0.y * k + width + time_step ) {
      color *= strength;
    }

    gl_FragColor = color;
  }
}%
