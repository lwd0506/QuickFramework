/**
 * @description 语言包用到，定义好之前，请不要随意修改顺序，以免读取语言包错误
 */
export enum Bundles {
    aimLine,
    eliminate,
    loadTest,
    netTest,
    nodePoolTest,
    tankBattle,
    shaders,
    resources,
    hall,
}
